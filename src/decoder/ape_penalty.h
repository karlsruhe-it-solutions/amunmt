#pragma once

#include <vector>

#include "common/file_stream.h"
#include "common/types.h"

#include "decoder/scorer.h"
#include "decoder/sentence.h"
#include "decoder/loader.h"

#include "mblas/matrix.h"

typedef std::vector<Word> SrcTrgMap;
typedef std::vector<float> Penalties;

////////////////////////////////////////////////////
class ApePenaltyState : public State {
  // Dummy, this scorer is stateless
};

////////////////////////////////////////////////////
class ApePenalty : public Scorer {
  private:
    const SrcTrgMap& srcTrgMap_;
    const Penalties& penalties_;

  public:
    ApePenalty(const std::string& name,
               const YAML::Node& config,
               size_t tab,
               const SrcTrgMap& srcTrgMap,
               const Penalties& penalties);

    // @TODO: make this work on GPU
    virtual void SetSource(const Sentence& source);
    // @TODO: make this work on GPU
    virtual void Score(
    		const State& in,
    		mblas::BaseMatrix& prob,
			State& out);

    virtual State* NewState();
    virtual void BeginSentenceState(State& state);

    virtual void AssembleBeamState(const State& in,
                                   const Beam& beam,
                                   State& out);

    virtual size_t GetVocabSize() const;
    mblas::BaseMatrix *CreateMatrix();
    void Filter(const std::vector<size_t>& filterIds);

  private:
    std::vector<float> costs_;
};

////////////////////////////////////////////////////
class ApePenaltyLoader : public Loader {
  public:
    ApePenaltyLoader(const std::string& name,
                     const YAML::Node& config);
    virtual void Load();
    virtual ScorerPtr NewScorer(size_t taskId);

  private:
    SrcTrgMap srcTrgMap_;
    Penalties penalties_;
};
