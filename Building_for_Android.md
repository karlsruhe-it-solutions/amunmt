These instructions were tested on a clean Ubuntu 16.04 container.

```shell
cd .. # go into parent directory of amunmt/ git clone

### setup NDK / Toolchain

# see https://developer.android.com/ndk/downloads/index.html
# probably needs exactly ndk version r12b!

wget https://dl.google.com/android/repository/android-ndk-r12b-linux-x86_64.zip -O ndk.zip
echo "170a119bfa0f0ce5dc932405eaa3a7cc61b27694 ndk.zip" | sha1sum -c -

unzip ndk.zip

pushd android-ndk-r12b
export tc=$PWD/../toolchain
build/tools/make_standalone_toolchain.py --arch=arm --install-dir=$tc --stl=gnustl --api=16
popd

### get boost

# source: https://github.com/emileb/Boost-for-Android-Prebuilt
# source: https://github.com/sorccu/Boost-for-Android
git clone https://github.com/emileb/Boost-for-Android-Prebuilt
pushd Boost-for-Android-Prebuilt/boost_1_53_0
# copy boost libs into toolchain
rsync armeabi-v7a/lib/ -aiP $tc/sysroot/usr/lib/
rsync include/ -aiP $tc/sysroot/usr/include/
popd

### configure and build

mkdir amunmt/build
cd amunmt/build
cmake \
 -DCMAKE_TOOLCHAIN_FILE=../cmake/android.toolchain.cmake \
 -DANDROID_STANDALONE_TOOLCHAIN=$tc \
 -DCMAKE_BUILD_TYPE=Debug \
 -DBoost_COMPILER=-gcc \
 ..

make -j$(nproc)

# binary
```
