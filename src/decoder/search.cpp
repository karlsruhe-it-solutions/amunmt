#include "search.h"

#include <memory>
#include <chrono>
#include <vector>
#include <boost/iterator/permutation_iterator.hpp>

#include "decoder/god.h"
#include "decoder/sentence.h"
#include "decoder/history.h"
#include "decoder/encoder_decoder.h"
#include "common/filter.h"
#include "mblas/base_matrix.h"

Search::Search(size_t threadId)
  : scorers_(God::GetScorers(threadId)),
    filterIndices_(scorers_[0]->GetVocabSize()) {}

size_t Search::MakeFilter(const Words& srcWords, const size_t vocabSize) {
  filterIndices_ = God::GetFilter().GetFilteredVocab(srcWords, vocabSize);
  for(size_t i = 0; i < scorers_.size(); i++) {
      scorers_[i]->Filter(filterIndices_);
  }
  return filterIndices_.size();
}

History Search::Decode(const Sentence& sentence)
{
  std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

  size_t beamSize = God::Get<size_t>("beam-size");
  bool normalize = God::Get<bool>("normalize");

  History history;

  Beam prevHyps = { history.NewHypothesis() };
  history.Add(prevHyps);

  States states(scorers_.size());
  States nextStates(scorers_.size());
  mblas::BaseMatrices probs(scorers_.size());

  size_t vocabSize = scorers_[0]->GetVocabSize();

  bool filter = God::Get<std::vector<std::string>>("softmax-filter").size();
  if(filter) {
    vocabSize = MakeFilter(sentence.GetWords(), vocabSize);
  }

  for(size_t i = 0; i < scorers_.size(); i++) {
	Scorer &scorer = *scorers_[i];
	scorer.SetSource(sentence);

    states[i].reset(scorer.NewState());
    nextStates[i].reset(scorer.NewState());

    scorer.BeginSentenceState(*states[i]);

	probs[i] = scorer.CreateMatrix();
  }

  const size_t maxLength = sentence.GetWords().size() * 3;
  do {
    for (size_t i = 0; i < scorers_.size(); i++) {
		Scorer &scorer = *scorers_[i];
		mblas::BaseMatrix &prob = *probs[i];

		prob.Resize(beamSize, vocabSize);
		scorer.Score(*states[i], prob, *nextStates[i]);
    }

    Beam hyps;

	const mblas::BaseMatrix &firstMatrix = *probs[0];

	firstMatrix.BestHyps(hyps, prevHyps, probs, beamSize, history, scorers_, filterIndices_);
    history.Add(hyps, history.size() == maxLength);

    Beam survivors;
    for(auto h : hyps) {
      if(h->GetWord() != EOS)
        survivors.push_back(h);
    }
    beamSize = survivors.size();
    if(beamSize == 0)
      break;

    for(size_t i = 0; i < scorers_.size(); i++)
      scorers_[i]->AssembleBeamState(*nextStates[i], survivors, *states[i]);

    prevHyps.swap(survivors);

  } while(history.size() <= maxLength);

  std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
  std::chrono::duration<double> fp_s = end - start;
  LOG(progress) << "Line " << sentence.GetLine()
    << ": Search took " << fp_s.count()
    << "s";

  for(size_t i = 0; i < scorers_.size(); i++) {
	  Scorer &scorer = *scorers_[i];
	  scorer.CleanUpAfterSentence();

	  mblas::BaseMatrix *prob = probs[i];
	  delete prob;
  }

  return history;
}



